package com.example.andrey.tracker.activites;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.andrey.tracker.R;
import com.example.andrey.tracker.db.DataBaseHelper;
import com.example.andrey.tracker.toolsAndContains.DbConstants;

import java.text.MessageFormat;

public class ResultActivity extends AppCompatActivity implements View.OnClickListener {

    private LinearLayout conteinerLinearLayout;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);


        conteinerLinearLayout = findViewById(R.id.conteinerLinerLayout);
        Button button = findViewById(R.id.btnClear);
        button.setOnClickListener(this);

        DataBaseHelper dataBaseHelper = new DataBaseHelper(this);
        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
//        Узнать нужно ли апдейтДата!!!!

        Cursor cursor = db.query(DbConstants.TABLE_NAME, null, null, null,
                null, null, null);

        if (cursor != null) {
            int idPosicion = cursor.getColumnIndex(DbConstants.FIELD_ID);
            int timePosicion = cursor.getColumnIndex(DbConstants.FIELD_TIME);
            int speedPosicion = cursor.getColumnIndex(DbConstants.FIELD_SPEED);
            int distancePosicion = cursor.getColumnIndex(DbConstants.FIELD_DISTANCE);
            if (cursor.moveToFirst()) {
                LinearLayout.LayoutParams params =
                        new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT);

                int topAndBottomMargin = (int) getResources()
                        .getDimension(R.dimen.top_and_bottom_margin_for_list);


                do {

                    long id = cursor.getLong(idPosicion);
                    double time = cursor.getDouble(timePosicion);
                    double speed = cursor.getDouble(speedPosicion);
                    double disctance = cursor.getDouble(distancePosicion);


                    TextView textView = new TextView(this);
                    params.setMargins(0, topAndBottomMargin, 0, topAndBottomMargin);


                    textView.setLayoutParams(params);
                    textView.setText(MessageFormat.format("'{'{0} {1}'}'\n{2}\n{0} - {3}",
                            time, speed, disctance));
                    textView.setTag(id);
                    textView.setOnClickListener(this);
                    conteinerLinearLayout.addView(textView);

                } while (cursor.moveToFirst());
            }
            cursor.close();
        }
        db.close();


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnClear:
                DataBaseHelper dataBaseHelper = new DataBaseHelper(this);
                SQLiteDatabase db = dataBaseHelper.getWritableDatabase();
                db.delete(DbConstants.TABLE_NAME, null, null);
                db.close();
                break;


        }

    }
}
