package com.example.andrey.tracker.activites;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.andrey.tracker.R;
import com.example.andrey.tracker.toolsAndContains.GeoUtils;

public class StartActivity extends AppCompatActivity implements View.OnClickListener, GeoUtils.HereInterface {

    TextView viewspeed;
    TextView viewTime;
    TextView viewdistance;

    double hereTime;
    double hereSpeed;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        viewspeed = findViewById(R.id.viewSpeed);
        viewTime = findViewById(R.id.viewTime);
        viewdistance = findViewById(R.id.viewDistance);
        Button buttonStart = findViewById(R.id.btnStart);
        buttonStart.setOnClickListener(this);
        Button buttonStop = findViewById(R.id.btnStop);
        buttonStop.setOnClickListener(this);
        findViewById(R.id.gt);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnStart:
                Toast.makeText(this, " dist", Toast.LENGTH_LONG).show();
                GeoUtils.getInstance().setListener(this);
                break;
            case R.id.btnStop:

                break;
        }


    }

    @Override
    protected void onDestroy() {
        GeoUtils.getInstance().setListener(this);
        super.onDestroy();
    }

    @Override
    public void timeHere(double time) {
        Toast.makeText(this, " получение тайм", Toast.LENGTH_LONG).show();
        viewTime.setText((int) hereTime);

    }

    @Override
    public void speedHere(double speed) {
        Toast.makeText(this, " получение время", Toast.LENGTH_LONG).show();
        viewspeed.setText((int) hereSpeed);

    }

    @Override
    public void distHere(float dist) {
        viewdistance.setText((int) dist);
    }

}
