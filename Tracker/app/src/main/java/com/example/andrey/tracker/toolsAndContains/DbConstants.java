package com.example.andrey.tracker.toolsAndContains;

public class DbConstants {
    public static final String DB_NAME = "result_db";
    public static final int DB_VERSION = 1;

    public static final String TABLE_NAME = "_ResultInfo";
    public static final String FIELD_ID = "_id";
    public static final String FIELD_TIME="_time";
    public static final String FIELD_SPEED="_tpeed";
    public static final String FIELD_DISTANCE="_distance";
}
