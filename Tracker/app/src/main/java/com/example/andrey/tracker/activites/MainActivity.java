package com.example.andrey.tracker.activites;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.andrey.tracker.R;
import com.example.andrey.tracker.db.DataBaseHelper;
import com.example.andrey.tracker.toolsAndContains.AppSettings;
import com.example.andrey.tracker.toolsAndContains.DbConstants;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int REQUEST_CODE_TC = 1001;


    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        findViewById(R.id.btnStart).setOnClickListener(this);
        findViewById(R.id.btnMap).setOnClickListener(this);
        findViewById(R.id.btnResult).setOnClickListener(this);

        AppSettings appSettings = new AppSettings(this);
        if (appSettings.isNeedShowTc()) {
            Intent intent = new Intent(this, TandCActivity.class);
            startActivityForResult(intent, REQUEST_CODE_TC);


            DataBaseHelper dataBaseHelper = new DataBaseHelper(this);
            SQLiteDatabase db = dataBaseHelper.getWritableDatabase();

            for (int i = 0; i < 10; i++) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DbConstants.FIELD_TIME, "Time" + 1);
                contentValues.put(DbConstants.FIELD_SPEED, "Speed" + 1);
                contentValues.put(DbConstants.FIELD_DISTANCE, "Distance" + 1);
                db.insert(DbConstants.TABLE_NAME, null, contentValues);
                long id = db.insert(DbConstants.TABLE_NAME, null, contentValues);
            }
            db.close();
        }

//        DataBaseHelper dataBaseHelper = new DataBaseHelper(this);
//        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
//
//        Cursor cursor = db.query(DbConstants.TABLE_NAME, null, null, null,
//                null, null, null);
//
//        if (cursor != null) {
//            int idPosicion = cursor.getColumnIndex(DbConstants.FIELD_ID);
//            int timePosicion = cursor.getColumnIndex(DbConstants.FIELD_TIME);
//            int speedPosicion = cursor.getColumnIndex(DbConstants.FIELD_SPEED);
//            int distancePosicion = cursor.getColumnIndex(DbConstants.FIELD_DISTANCE);
//            if (cursor.moveToFirst()) {
//
//                do {
//
//                    long id = cursor.getLong(idPosicion);
//                    double time = cursor.getDouble(timePosicion);
//                    double speed = cursor.getDouble(speedPosicion);
//                    double disctance = cursor.getDouble(distancePosicion);
//
//                } while (cursor.moveToFirst());
//            }
//            cursor.close();
//        }
//        db.close();

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Hello friend");
        builder.setPositiveButton("Hi", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog alertDialog = builder.show();
        builder.setPositiveButton("Hi", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(MainActivity.this, "good day", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_TC) {
            AppSettings appSettings = new AppSettings(this);
            if (appSettings.isNeedShowTc()) {
                Toast.makeText(this, "you did not accept the agreement", Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnStart:
                startActivity(new Intent(this, StartActivity.class));
                break;
            case R.id.btnResult:
                startActivity(new Intent(this, ResultActivity.class));
        }

    }
}
