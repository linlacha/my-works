package com.example.andrey.tracker.activites;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.example.andrey.tracker.R;
import com.example.andrey.tracker.toolsAndContains.AppSettings;

public class TandCActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_t_and_c);

        findViewById(R.id.btnOkTandCActivity).setOnClickListener(this);
        findViewById(R.id.btnCancelTandCActivity).setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnOkTandCActivity){
            AppSettings appSettings = new AppSettings(this);
            appSettings.setISNeedShowTC(false);
        }
        finish();
    }
}
