package com.example.andrey.tracker.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.andrey.tracker.toolsAndContains.DbConstants;

public class DataBaseHelper extends SQLiteOpenHelper {
    public DataBaseHelper(Context context) {
        super(context, DbConstants.DB_NAME, null, DbConstants.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(" CREATE TABLE " + DbConstants.TABLE_NAME + " (" +
                DbConstants.FIELD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DbConstants.FIELD_TIME + " DOUBLE NOT NULL, " +
                DbConstants.FIELD_SPEED + " DOUBLE NOT NULL, " +
                DbConstants.FIELD_DISTANCE + "DOUBLE NOT NULL); ");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
