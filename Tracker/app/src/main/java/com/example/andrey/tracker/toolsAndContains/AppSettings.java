package com.example.andrey.tracker.toolsAndContains;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.prefs.PreferenceChangeEvent;

public class AppSettings {
    private static final String KEY_BOOLEAN_IS_NEED_SHOW_TC = "KEY_BOOLEAN_IS_NEED_SHOW_TC";
    private SharedPreferences sharedPreferences;

    public AppSettings(Context context) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void setISNeedShowTC(boolean ISNeedShowTC) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(KEY_BOOLEAN_IS_NEED_SHOW_TC, ISNeedShowTC);
        editor.commit();
    }

    public boolean isNeedShowTc() {
        return sharedPreferences.getBoolean(KEY_BOOLEAN_IS_NEED_SHOW_TC, true);
    }
}
