package com.example.andrey.tracker.toolsAndContains;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

public class GeoUtils extends AppCompatActivity implements LocationListener {


    public static Location imHere;
    private HereInterface listener;
    private static GeoUtils instance;
    Location oldLocation;
    float dist = 0;

    private GeoUtils() {
    }

    public static GeoUtils getInstance() {
        if (instance == null) {
            instance = new GeoUtils();
        }
        return instance;
    }


    public void setUpLocationListener(Context context) {
        LocationManager locationManager = (LocationManager)
                context.getSystemService(Context.LOCATION_SERVICE);
        Log.d("lacha", "1");

        Log.d("lacha", "2");
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)

        {

            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Log.d("lacha", "3 ->");
        assert locationManager != null;
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                500, 5, this);


        Log.d("lacha", "bugi");
        imHere = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        Log.d("lacha", "3.1");
//        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 10,
//                locationListener);
//        Log.d("lacha", "bugi1");
//        imHere = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
    }

    private float getDistancBetweenTwoPoints(double lat1, double lat2, double lon1, double lon2) {
        float[] result = new float[1];
        Toast.makeText(this, "getDistancBetweenTwoPoints", Toast.LENGTH_LONG).show();
        Location.distanceBetween(lat1, lat2, lon1, lon2, result);
        return result[0];
    }

    @Override
    public void onLocationChanged(Location location) {

        imHere = location;
        Toast.makeText(this, "getSpeed", Toast.LENGTH_LONG).show();
        double mySpeed = location.getSpeed();
        Toast.makeText(this, "getTime", Toast.LENGTH_LONG).show();
        double myTime = location.getTime();
        if (oldLocation != null) {
            Toast.makeText(this, " dist", Toast.LENGTH_LONG).show();
            dist = getDistancBetweenTwoPoints(oldLocation.getLatitude(), oldLocation.getLongitude(),
                    oldLocation.getLatitude(), oldLocation.getLongitude());
        }


        if (listener != null) {
            listener.speedHere(mySpeed);
        }
        if (listener != null) {
            listener.timeHere(myTime);
        }
        if (listener != null) {
            listener.distHere(dist);
//            узнать правильно ли я перенес дист
        }

    }


    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {


    }


    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    public void setListener(HereInterface listener) {
        Log.d("lacha", "6");
        this.listener = listener;
    }

    public interface HereInterface {
        void timeHere(double time);

        void speedHere(double speed);

        void distHere(float dist);
    }
}
