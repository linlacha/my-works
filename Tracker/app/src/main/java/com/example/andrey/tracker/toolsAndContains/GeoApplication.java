package com.example.andrey.tracker.toolsAndContains;

import android.app.Application;
import android.util.Log;

public class GeoApplication extends Application {

    public void onCreate() {
        super.onCreate();
        Log.d("lacha", "geoApp");
        GeoUtils.getInstance().setUpLocationListener(this);
    }
}
