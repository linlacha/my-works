package com.lacha;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;
import com.badlogic.gdx.math.Rectangle;

import java.util.Iterator;

public class PlayState implements Screen {

    final RunMario game;

    OrthographicCamera camera;
    SpriteBatch batch;
    Texture bg;
    Texture marioImage;
    Texture coinsImage;
    Texture bombImage;
    Music musicMario;
    Sound soundCoins;
    Sound soundBomb;
    Rectangle mario;
    Vector3 toutchPos;
    Array<Rectangle> coindrops;
    long lastDropTime;
    Array<Rectangle> bombdrops;
    int dropsGatcered;



    public PlayState(final RunMario gam) {
        this.game = gam;
        camera = new OrthographicCamera();
        camera.setToOrtho(false, 800, 480);


        batch = new SpriteBatch();


        toutchPos = new Vector3();

        bg = new Texture("bgmario.png");
        marioImage = new Texture("mario.png");
        coinsImage = new Texture("coins.jpg");
        bombImage = new Texture("bomb.jpg");
        bg = new Texture("bgmario.png");

        musicMario = Gdx.audio.newMusic(Gdx.files.internal("musicMario.mp3"));
        soundCoins = Gdx.audio.newSound(Gdx.files.internal("soundCoins.wav"));
        soundBomb = Gdx.audio.newSound(Gdx.files.internal("GameOver.wav"));

        musicMario.setLooping(true);
        musicMario.play();

        mario = new Rectangle();
        mario.x = 800 / 2 - 64 / 2;
        mario.y = 20;
        mario.width = 64;
        mario.height = 64;

        coindrops = new Array<Rectangle>();
        spawnCoinDrop();

        bombdrops = new Array<Rectangle>();
        spawnBombDrop();
    }

    private void spawnCoinDrop() {
        Rectangle coindrop = new Rectangle();
        coindrop.x = MathUtils.random(0, 800 - 64);
        coindrop.y = 480;
        coindrop.width = 64;
        coindrop.height = 64;
        coindrops.add(coindrop);
        lastDropTime = TimeUtils.nanoTime();
    }

    private void spawnBombDrop() {
        Rectangle bombdrop = new Rectangle();
        bombdrop.x = MathUtils.random(0, 800 - 64);
        bombdrop.y = 480;
        bombdrop.width = 64;
        bombdrop.height = 64;
        bombdrops.add(bombdrop);
        lastDropTime = TimeUtils.nanoTime();
    }


    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        camera.update();
        batch.setProjectionMatrix(camera.combined);

        game.batch.begin();
        game.batch.draw(bg, 0,0, RunMario.WIDHT, RunMario.HEIGHT);
        game.font.draw(game.batch, "coins: " + dropsGatcered, 0, 480);
        game.batch.draw(marioImage, mario.x, mario.y);
        for (Rectangle coindrop : coindrops) {
            game.batch.draw(coinsImage, coindrop.x, coindrop.y);
        }
        for (Rectangle bombdrop : bombdrops) {
            game.batch.draw(bombImage, bombdrop.x, bombdrop.y);
        }

        game.batch.end();

        if (Gdx.input.isTouched()) {
            toutchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
            camera.unproject(toutchPos);
            mario.x = (int) (toutchPos.x - 64 / 2);
        }
        if (mario.x < 0) mario.x = 0;
        if (mario.x > 800 - 64) mario.x = 800 - 64;

        if (TimeUtils.nanoTime() - lastDropTime > 1000000000) spawnCoinDrop();
        if (TimeUtils.nanoTime() - lastDropTime > 1000000000) spawnBombDrop();

        Iterator<Rectangle> iter = coindrops.iterator();
        while (iter.hasNext()) {
            Rectangle coindrop = iter.next();
            coindrop.y -= 200 * Gdx.graphics.getDeltaTime();
            if (coindrop.y + 64 < 0) iter.remove();
            if (coindrop.overlaps(mario)) {
                dropsGatcered++;
                soundCoins.play();
                iter.remove();
            }
        }

        Iterator<Rectangle> iterator = bombdrops.iterator();
        while (iterator.hasNext()) {
            Rectangle bombdrop = iterator.next();
            bombdrop.y -= 200 * Gdx.graphics.getDeltaTime();
            if (bombdrop.y + 64 < 0) iterator.remove();
            if (bombdrop.overlaps(mario)) {
                soundBomb.play();
                iterator.remove();
            }
        }

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        batch.dispose();
        marioImage.dispose();
        musicMario.dispose();
        coinsImage.dispose();
        soundCoins.dispose();
        soundBomb.dispose();
        bombImage.dispose();

    }

    @Override
    public void show() {
        musicMario.play();
    }
}
