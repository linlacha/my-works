package com.lacha;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.lacha.State.MenuState;

import java.awt.font.TextLayout;

public class RunMario extends Game {

    public BitmapFont font;
    public static final int WIDHT = 800;
    public static final int HEIGHT = 480;
    private Music musicMario;
    public SpriteBatch batch;


    @Override
    public void create() {
        batch = new SpriteBatch();
        font = new BitmapFont();
        this.setScreen(new MenuState(this));
        musicMario = Gdx.audio.newMusic(Gdx.files.internal("musicMario.mp3"));

        musicMario.setLooping(true);
        musicMario.setVolume(0.4f);
        musicMario.play();
    }

    @Override
    public void render() {
        super.render();
    }

    @Override
    public void pause() {
        super.pause();
    }

    @Override
    public void resume() {
        super.resume();
    }

    @Override
    public void dispose() {
        super.dispose();
        batch.dispose();
        font.dispose();
        musicMario.dispose();

    }
}
