package com.lacha.State;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.lacha.RunMario;

public class MenuState implements Screen {

    final RunMario game;
    OrthographicCamera camera;
    Texture bg;
    Texture btnplay;

    public MenuState(RunMario game) {
        this.game = game;

        camera = new OrthographicCamera();
        camera.setToOrtho(false, 800, 480);
        bg = new Texture("bgmario.png");
        btnplay = new Texture("btnPlay.jpg");

    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        camera.update();
        game.batch.setProjectionMatrix(camera.combined);

        game.batch.begin();
        game.batch.draw(bg, 0, 0, RunMario.WIDHT, RunMario.HEIGHT);
        game.batch.draw(btnplay, (RunMario.WIDHT / 2) - (btnplay.getWidth() / 2), RunMario.HEIGHT / 2);
        game.font.draw(game.batch, "CATCH COINS", 375, 450);
        game.font.draw(game.batch, "CLICK PLAY", 375, 400);

        if (Gdx.input.isTouched())
            game.setScreen(new PlayState(game));
        dispose();

        game.batch.end();

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
    }
}
